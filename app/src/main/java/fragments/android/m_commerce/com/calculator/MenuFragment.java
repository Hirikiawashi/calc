package fragments.android.m_commerce.com.calculator;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class MenuFragment extends Fragment {

    private IFragmentCotainer fragmentContainer;
    public MenuFragment(){}

    @Override
    public void onAttach(Activity activity){

        super.onAttach(activity);
        fragmentContainer = (IFragmentCotainer)activity;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState){

        View view = inflater.inflate(R.layout.menu_fragment,container,false);

        Button btnSimple = (Button) view.findViewById(R.id.buttonSimple);
        Button btnAdvance = (Button) view.findViewById(R.id.buttonAdvance);
        Button btnAbout = (Button) view.findViewById(R.id.buttonAbout);
        Button btnExit = (Button) view.findViewById(R.id.buttonExit);

        btnSimple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentContainer.replaceFragment(new SimpleCalc());
            }
        });

        btnAdvance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentContainer.replaceFragment(new AdvancedCalc());
            }
        });

        btnAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentContainer.replaceFragment(new About());
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
                System.exit(0);
            }
        });

        return view;
    }
}

