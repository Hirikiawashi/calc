package fragments.android.m_commerce.com.calculator;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SimpleCalc extends Fragment implements View.OnClickListener {

    Button but1, but2, but3,
            but4, but5, but6,
            but7, but8, but9,
            but0, butBksp, butC,
            butPlusMinus, butEquals, butDiv,
            butMultiply, butDot, butPlus, butMinus, Pi;

    public EditText display;

    String operation = "clear";
    float temp = 0;
    float number1 = 0;
    float number2 = 0;
    float result= 0;

    public SimpleCalc() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.simple_calc, container, false);
        but1 = (Button) rootView.findViewById(R.id.but1);
        but2 = (Button) rootView.findViewById(R.id.but2);
        but3 = (Button) rootView.findViewById(R.id.but3);
        but4 = (Button) rootView.findViewById(R.id.but4);
        but5 = (Button) rootView.findViewById(R.id.but5);
        but6 = (Button) rootView.findViewById(R.id.but6);
        but7 = (Button) rootView.findViewById(R.id.but7);
        but8 = (Button) rootView.findViewById(R.id.but8);
        but9 = (Button) rootView.findViewById(R.id.but9);
        but0 = (Button) rootView.findViewById(R.id.but0);
        Pi = (Button)   rootView.findViewById(R.id.Pi);
        butBksp = (Button) rootView.findViewById(R.id.butBks);
        butC = (Button) rootView.findViewById(R.id.butC);
        butPlusMinus = (Button) rootView.findViewById(R.id.butZnak);
        butEquals = (Button) rootView.findViewById(R.id.butRowna);
        butDiv = (Button) rootView.findViewById(R.id.butDziel);
        butMultiply = (Button) rootView.findViewById(R.id.butMnoz);
        butDot = (Button) rootView.findViewById(R.id.butKropka);
        butPlus = (Button) rootView.findViewById(R.id.butDodaj);
        butMinus = (Button) rootView.findViewById(R.id.butOdejmij);
        display = (EditText) rootView.findViewById(R.id.display);
        display.setKeyListener(null);

        but1.setOnClickListener(this);
        but2.setOnClickListener(this);
        but3.setOnClickListener(this);
        but4.setOnClickListener(this);
        but5.setOnClickListener(this);
        but6.setOnClickListener(this);
        but7.setOnClickListener(this);
        but8.setOnClickListener(this);
        but9.setOnClickListener(this);
        but0.setOnClickListener(this);
        Pi.setOnClickListener(this);
        butBksp.setOnClickListener(this);
        butC.setOnClickListener(this);
        butPlusMinus.setOnClickListener(this);
        butEquals.setOnClickListener(this);
        butDiv.setOnClickListener(this);
        butMultiply.setOnClickListener(this);
        butDot.setOnClickListener(this);
        butPlus.setOnClickListener(this);
        butMinus.setOnClickListener(this);

        return rootView;
    }


    public void operation()
    {

        if (operation.equals("+")) {
            result = temp + Float.parseFloat(display.getText().toString().substring(1));
            display.setText(Float.toString(result));
        }

        else if (operation.equals("-")) {
            result = temp - Float.parseFloat(display.getText().toString().substring(1));
            display.setText(Float.toString(result));
        }


        else if (operation.equals("*")) {
            result = temp * Float.parseFloat(display.getText().toString().substring(1));
            display.setText(Float.toString(result));
        }

        else if (operation.equals("/")) {
            if(display.getText().toString().substring(1).equals("0")){
                Context context = getActivity().getApplicationContext();
                Toast t =  Toast.makeText(context, "ERROR Nie dziel przez 0", Toast.LENGTH_LONG);
                t.show();
            }
            result = temp / Float.parseFloat(display.getText().toString().substring(1));
            display.setText(Float.toString(result));
        }
        else if (operation.equals("Clear")) {
            display.setText("0");
        }
    }


    @Override
    public void onClick(View arg0) {
        Editable str  = display.getText();
        String temp="0";
        switch (arg0.getId()) {
            case R.id.but1:
                if (number2 != 0) {
                    number2 = 0;
                    display.setText("");
                }
                temp = String.valueOf(display.getText());
                if(temp.contains("0.")){
                    str = str.append(but1.getText());
                    display.setText(str);
                }
                else if (temp.startsWith("0")){
                    display.setText(but1.getText());
                }
                else
                {
                    str = str.append(but1.getText());
                    display.setText(str);
                }
                break;
            case R.id.but2:
                if (number2 != 0) {
                    number2 = 0;
                    display.setText("");
                }
                temp = String.valueOf(display.getText());
                if(temp.contains("0.")){
                    str = str.append(but2.getText());
                    display.setText(str);
                }
                else if (temp.startsWith("0")){
                    display.setText(but2.getText());
                }
                else
                {
                    str = str.append(but2.getText());
                    display.setText(str);
                }
                break;
            case R.id.but3:
                if (number2 != 0) {
                    number2 = 0;
                    display.setText("");
                }
                temp = String.valueOf(display.getText());
                if(temp.contains("0.")){
                    str = str.append(but3.getText());
                    display.setText(str);
                }
                else if (temp.startsWith("0")){
                    display.setText(but3.getText());
                }
                else
                {
                    str = str.append(but3.getText());
                    display.setText(str);
                }
                break;
            case R.id.but4:
                if (number2 != 0) {
                    number2 = 0;
                    display.setText("");
                }
                temp = String.valueOf(display.getText());
                if(temp.contains("0.")){
                    str = str.append(but4.getText());
                    display.setText(str);
                }
                else if (temp.startsWith("0")){
                    display.setText(but4.getText());
                }
                else
                {
                    str = str.append(but4.getText());
                    display.setText(str);
                }
                break;
            case R.id.but5:
                if (number2 != 0) {
                    number2 = 0;
                    display.setText("");
                }
                temp = String.valueOf(display.getText());
                if(temp.contains("0.")){
                    str = str.append(but5.getText());
                    display.setText(str);
                }
                else if (temp.startsWith("0")){
                    display.setText(but5.getText());
                }
                else
                {
                    str = str.append(but5.getText());
                    display.setText(str);
                }
                break;
            case R.id.but6:
                if (number2 != 0) {
                    number2 = 0;
                    display.setText("");
                }
                temp = String.valueOf(display.getText());
                if(temp.contains("0.")){
                    str = str.append(but6.getText());
                    display.setText(str);
                }
                else if (temp.startsWith("0")){
                    display.setText(but6.getText());
                }
                else
                {
                    str = str.append(but6.getText());
                    display.setText(str);
                }
                break;
            case R.id.but7:
                if (number2 != 0) {
                    number2 = 0;
                    display.setText("");
                }
                temp = String.valueOf(display.getText());
                if(temp.contains("0.")){
                    str = str.append(but7.getText());
                    display.setText(str);
                }
                else if (temp.startsWith("0")){
                    display.setText(but7.getText());
                }
                else
                {
                    str = str.append(but7.getText());
                    display.setText(str);
                }
                break;
            case R.id.but8:
                if (number2 != 0) {
                    number2 = 0;
                    display.setText("");
                }
                temp = String.valueOf(display.getText());
                if(temp.contains("0.")){
                    str = str.append(but8.getText());
                    display.setText(str);
                }
                else if (temp.startsWith("0")){
                    display.setText(but8.getText());
                }
                else
                {
                    str = str.append(but8.getText());
                    display.setText(str);
                }
                break;
            case R.id.but9:
                if (number2 != 0) {
                    number2 = 0;
                    display.setText("");
                }
                temp = String.valueOf(display.getText());
                if(temp.contains("0.")){
                    str = str.append(but9.getText());
                    display.setText(str);
                }
                else if (temp.startsWith("0")){
                    display.setText(but9.getText());
                }
                else
                {
                    str = str.append(but9.getText());
                    display.setText(str);
                }
                break;
            case R.id.Pi:
                if (number2 != 0) {
                    number2 = 0;
                    display.setText("");
                }
                temp = String.valueOf(display.getText());
                if(temp.contains("0.")){
                    str = str.append(Pi.getText());
                    display.setText(str);
                }
                else if(temp.startsWith("0")){
                    display.setText(Pi.getText());
                }
                else
                {
                    str = str.append(Pi.getText());
                    display.setText(str);
                }
                break;
            case R.id.but0:
                if (number2 != 0) {
                    number2 = 0;
                    display.setText("");
                }
                temp = String.valueOf(display.getText());
                if(temp.contains("0.")){
                    str = str.append(but0.getText());
                    display.setText(str);
                }
                else if(temp.startsWith("0")){
                }
                else
                {
                    str = str.append(but0.getText());
                    display.setText(str);
                };
                break;
            case R.id.butC:
                this.temp = 0;
                number1 = 0;
                number2 = 0;
                result=0;
                display.setText("0");
                operation = "clear";
                temp="0 ";
                break;
            case R.id.butKropka:
                temp = String.valueOf(display.getText());
                if(!temp.contains("."))
                {
                    str = str.append(".");
                    display.setText(str);
                }
                break;

            case R.id.butBks:
                temp = String.valueOf(display.getText());
                if(temp.length()>0)
                {
                    display.setText(temp.substring(0, temp.length() - 1));
                }
                break;
            case R.id.butDodaj:
                operation = "+";
                this.temp = Float.parseFloat(display.getText().toString());
                display.setText("+");
                break;
            case R.id.butOdejmij:
                operation = "-";
                this.temp = Float.parseFloat(display.getText().toString());
                display.setText("-");
                break;
            case R.id.butMnoz:
                operation = "*";
                this.temp = Float.parseFloat(display.getText().toString());
                display.setText("*");
                break;
            case R.id.butDziel:
                operation = "/";
                this.temp = Float.parseFloat(display.getText().toString());
                display.setText("/");
                break;
            case R.id.butRowna:
                operation();
                break;
            case R.id.butZnak:
                temp = String.valueOf(display.getText());
                float a = Float.parseFloat(temp);
                temp = String.valueOf(-a);
                display.setText(temp);
                break;

            default:
                display.setText("def");
        }

    }
}